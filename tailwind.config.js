module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
   // textColor: theme => theme('colors'),
    textColor: {
        
      headline1:'#ffffff',
      headline2:'#d9dae4',
      footercolor:'#ff6e43',
      textwhite:'#ffffff',
      formgrey:'#8c9dc5',
    },
    extend: {
    }, 
  },
  
  plugins: [],
}
